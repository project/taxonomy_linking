<?php

/**
 * @file
 * module admin.
 */


/**
 * Form builder: Configure form.
 */
function _taxonomy_autolink_settings_form() {
  $form = array();
  $form['taxonomy_autolink'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => 'env',
  );
  // Current settings status.
  $form['taxonomy_autolink']['status'] = array(
    '#type' => 'fieldset',
    '#weight' => -1,
    '#title' => t('Status'),
    '#group' => 'taxonomy_autolink',
  );
  $form['taxonomy_autolink']['condition'] = array(
    '#type' => 'fieldset',
    '#weight' => 1,
    '#title' => t('Display conditions'),
    '#description' => t('Apply taxonomy autolink to the following content types.'),
    '#group' => 'taxonomy_autolink',
  );
  $form['taxonomy_autolink']['condition']['nodetypes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $node_types = node_type_get_types();
  $node_names = node_type_get_names();
  if (is_array($node_names) && count($node_names)) {
    foreach ($node_names as $key => $value) {
      $form['taxonomy_autolink']['condition']['nodetypes']['taxonomy_autolink_' . $node_types[$key]->type . '_enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Apply taxonomy autolink to content type @value (only affects content type @value)', array(
          '@value' => $value,
        )),
        '#default_value' => variable_get('taxonomy_autolink_' . $node_types[$key]->type . '_enabled', FALSE),
      );
      $form['taxonomy_autolink']['condition']['nodetypes']['voc_' . $node_types[$key]->type] = array(
        '#type' => 'fieldset',
        '#title' => t('Please select'),
        '#collapsible' => TRUE,
        '#states' => array(
          'visible' => array(':input[name="' . 'taxonomy_autolink_' . $node_types[$key]->type . '_enabled' . '"]' => array('checked' => TRUE)),
        ),
      );
      $form['taxonomy_autolink']['condition']['nodetypes']['voc_' . $node_types[$key]->type]['taxonomy_autolink_' . $node_types[$key]->type . '_vocabs'] = array(
        '#type' => 'select',
        '#title' => t('Taxonomy vocabularies'),
        '#description' => t('Apply taxonomy autolink to above selected taxonomy vocabularies.'),
        '#options' => taxonomy_autolink_vocabs(),
        '#multiple' => TRUE,
        '#default_value' => variable_get('taxonomy_autolink_' . $node_types[$key]->type . '_vocabs', ''),
      );
    }
  }
  $form = system_settings_form($form);
  // Add a callback function.
  $form['#submit'][] = '_taxonomy_autolink_settings_form_callback';
  return $form;
}

/**
 * Callback function.
 * @param $form
 * @param $form_state
 */
function _taxonomy_autolink_settings_form_callback($form, $form_state) {
  // Perform callback actions.
}
